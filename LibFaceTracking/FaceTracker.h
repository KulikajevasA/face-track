#ifndef FACE_TRACKER_H
#define FACE_TRACKER_H

#include <opencv/cv.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <vector>
#include <thread>

#include "Contour.h"
#include "FeatureSorter.h"
#include "Color.h"

using namespace Tracker;

class FaceTracker
{
private:
	cv::VideoCapture m_videoCapture;
	int m_loCol[3], m_hiCol[3];
	cv::Mat m_loHSV, m_hiHSV;
	cv::Mat m_frame, m_hsv, m_mask, m_tmp;
	std::vector<std::vector<cv::Point>> m_contours;
	cv::Size m_blurKernel, m_morphKernel;
	cv::Mat m_morphStructure;
	std::vector<Contour> m_centers;
	int m_iCameraIndex;
	FeatureSorter *m_pFeatureSorter;
	bool m_bDebugContext;
	int m_iUpdateRate;

private:
	bool m_bIsRunning;
	std::thread m_thread;
	void RunThread(void);

public:
	FaceTracker(int cameraIndex, Color 
		loCol, Color hiCol, bool debug,
		int updateRateMS);
	void Start(FeatureSorter *sorter);
	void Stop(void);
	std::vector<Contour> *GetFeatures(void) { return &m_centers; }
};

#endif