#include "FaceTrackerWrapper.h"
#include "FaceTracker.h"

using namespace Tracker;

FaceTrackerWrapper::FaceTrackerWrapper(TrackSettingsT settings)
{
	auto tracker = new FaceTracker(settings.cameraIndex,
		settings.loCol, settings.hiCol, settings.debug,
		settings.updateRateMS);
	m_pFaceTracker = tracker;
	m_pFeatureSorter = new FeatureSorter(tracker->GetFeatures());
}

FaceTrackerWrapper::~FaceTrackerWrapper(void)
{
	delete m_pFaceTracker;
	delete m_pFeatureSorter;
}

void FaceTrackerWrapper::Start(void)
{
	FaceTracker *tracker = (FaceTracker*)m_pFaceTracker;
	tracker->Start(m_pFeatureSorter);
}

void FaceTrackerWrapper::Stop(void)
{
	FaceTracker *tracker = (FaceTracker*)m_pFaceTracker;
	tracker->Stop();
}