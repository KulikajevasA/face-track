#ifndef CONTOUR_H
#define CONTOUR_H

namespace Tracker
{
	struct Contour
	{
		float size;
		float x, y;
		bool dead;

		Contour()
		{
			size = 0;
			x = y = -1;
			dead = true;
		}

		Contour(float x, float y, float size)
		{
			this->size = size;
			this->x = x;
			this->y = y;
			dead = false;
		}
	};
}

#endif