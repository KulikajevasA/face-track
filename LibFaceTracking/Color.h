#ifndef COLOR_H
#define COLOR_H

namespace Tracker
{
	struct Color
	{
		int r, g, b;

		Color()
		{
			r = g = b = 0;
		}

		Color(int r, int g, int b)
		{
			this->r = r;
			this->g = g;
			this->b = b;
		}
	};
}
#endif
