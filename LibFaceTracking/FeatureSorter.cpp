#include "FeatureSorter.h"

#include <algorithm>

using namespace Tracker;

FeatureSorter::FeatureSorter(std::vector<Contour> *featureList)
{
	m_featureList = featureList;
}

void FeatureSorter::Sort()
{
	std::sort(m_featureList->begin(), m_featureList->end(),
		&FeatureSorter::ComparatorArea);

	auto it = m_featureList->size() >= 4
		? (m_featureList->begin() + 4) : m_featureList->end();

	std::sort(m_featureList->begin(), it, &ComparatorVertical);

	if (m_featureList->size() >= 3)
		std::sort(m_featureList->begin() + 1,
			m_featureList->begin() + 3, ComparatorHorizontal);

	size_t i;

	for (i = 0; i < m_featureList->size() && i < 4; i++)
		m_face[i] = m_featureList->at(i);

	for (i; i < 4; i++)
		m_face[i] = Contour();
}

bool FeatureSorter::ComparatorArea(Contour &a, Contour &b)
{
	return a.size < b.size;
}

bool FeatureSorter::ComparatorVertical(Contour &a, Contour &b)
{
	return a.y < b.y;
}

bool FeatureSorter::ComparatorHorizontal(Contour &a, Contour &b)
{
	return a.x < b.x;
}

Contour *FeatureSorter::GetAtIndex(int index)
{
	if (m_featureList && m_featureList->size() > index)
		return &m_featureList->at(index);
	return NULL;
}