#ifndef  FEATURE_SORTER_H
#define FEATURE_SORTER_H

#include "Contour.h"
#include <vector>

namespace Tracker
{
	class FeatureSorter
	{
	private:
		std::vector<Contour> *m_featureList;
		Contour m_face[4];

	public:
		FeatureSorter(std::vector<Contour> *featureList);
		void Sort();

		Contour *GetAtIndex(int index);
		Contour *GetLipTop(void) { return GetAtIndex(0); }
		Contour *GetLipLeft(void) { return GetAtIndex(1); }
		Contour *GetLipRight(void) { return GetAtIndex(2); }
		Contour *GetLipBottom(void) { return GetAtIndex(3); }

		Contour GetFaceIndex(int index) { return m_face[index]; };

	private:
		static bool ComparatorArea(Contour &a, Contour &b);
		static bool ComparatorVertical(Contour &a, Contour &b);
		static bool ComparatorHorizontal(Contour &a, Contour &b);
	};
}
#endif