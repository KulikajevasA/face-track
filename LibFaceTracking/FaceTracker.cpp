#include "FaceTracker.h"

void rgb2Hsv(int *inRGB, cv::Mat &outHSV)
{
	auto rgb = cv::Mat(1, 1, CV_8UC3, cv::Scalar(inRGB[0], inRGB[1], inRGB[2]));
	cv::cvtColor(rgb, outHSV, cv::COLOR_RGB2HSV);
}

FaceTracker::FaceTracker(int cameraIndex, 
	Color loCol, Color hiCol, bool debug,
	int updateRateMS)
{
	m_iCameraIndex = cameraIndex;
	m_bDebugContext = debug;
	m_iUpdateRate = updateRateMS;

	//m_loCol[0] = 92; m_loCol[1] = 138; m_loCol[2] = 0;
	//m_hiCol[0] = 61;  m_hiCol[1] = 22; m_hiCol[2] = 181;

	//m_loCol[0] = 70; m_loCol[1] = 132; m_loCol[2] = 142;
	//m_hiCol[0] = 114;  m_hiCol[1] = 0; m_hiCol[2] = 87;

	m_loCol[0] = loCol.r; m_loCol[1] = loCol.g; m_loCol[2] = loCol.b;
	m_hiCol[0] = hiCol.r; m_hiCol[1] = hiCol.g; m_hiCol[2] = hiCol.b;

	m_videoCapture = cv::VideoCapture();

	m_blurKernel = cv::Size(11, 11);
	m_morphKernel = cv::Size(13, 13);
	m_morphStructure = cv::getStructuringElement(cv::MORPH_RECT, m_morphKernel);
}

void FaceTracker::Start(FeatureSorter *sorter)
{
	m_pFeatureSorter = sorter;
	m_videoCapture.open(m_iCameraIndex);
	m_bIsRunning = true;
	m_thread = std::thread(&FaceTracker::RunThread, this);
}

void FaceTracker::Stop(void)
{
	m_pFeatureSorter = NULL;
	m_videoCapture.release();
	m_bIsRunning = false;
}

void FaceTracker::RunThread(void)
{
	while (m_bIsRunning)
	{
		m_centers.clear();

		if (!m_videoCapture.isOpened()) { cv::waitKey(1000); continue; }

		bool ret = m_videoCapture.read(m_frame);

		if (!ret) { cv::waitKey(1000); continue; }

		cv::Size size = m_frame.size();

		cv::GaussianBlur(m_frame, m_frame, m_blurKernel, 0);
		rgb2Hsv(m_loCol, m_loHSV);
		rgb2Hsv(m_hiCol, m_hiHSV);

		cv::cvtColor(m_frame, m_hsv, cv::COLOR_BGR2HSV);
		cv::inRange(m_hsv, m_loHSV, m_hiHSV, m_mask);

		cv::morphologyEx(m_mask, m_mask, cv::MORPH_CLOSE, m_morphStructure);
		cv::morphologyEx(m_mask, m_mask, cv::MORPH_OPEN, m_morphStructure);

		m_mask.copyTo(m_tmp);
		cv::findContours(m_tmp, m_contours,
			cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

		for (size_t i = 0; i < m_contours.size(); i++)
		{
			float area = (float)cv::contourArea(m_contours[i]);
			auto M = cv::moments(m_contours[i]);
			cv::Point2f point = cv::Point2f((float)(M.m10 / M.m00),
				(float)(M.m01 / M.m00));

			m_centers.push_back(Contour(point.x / size.width,
				point.y / size.height, area));

			if (m_bDebugContext)
				cv::circle(m_frame, point, 10, cv::Scalar(0, 0, 255));
		}

		if (m_pFeatureSorter)
		{
			m_pFeatureSorter->Sort();

			if (m_bDebugContext)
			{
				auto topLip = m_pFeatureSorter->GetLipTop();
				auto leftLip = m_pFeatureSorter->GetLipLeft();
				auto rightLip = m_pFeatureSorter->GetLipRight();
				auto bottomLip = m_pFeatureSorter->GetLipBottom();

				cv::circle(m_frame, cv::Point2f(20, 20), 3, cv::Scalar(0, 255, 0), 5, 2);
				cv::circle(m_frame, cv::Point2f(10, 30), 3, cv::Scalar(255, 0, 0), 5, 2);
				cv::circle(m_frame, cv::Point2f(30, 30), 3, cv::Scalar(255, 255, 0), 5, 2);
				cv::circle(m_frame, cv::Point2f(20, 40), 3, cv::Scalar(0, 255, 255), 5, 2);

				if (topLip)
				{
					cv::Point2f lip = cv::Point2f(topLip->x * size.width, topLip->y * size.height);
					cv::circle(m_frame, lip, 5, cv::Scalar(0, 255, 0), 5);
				}

				if (leftLip)
				{
					cv::Point2f lip = cv::Point2f(leftLip->x * size.width, leftLip->y * size.height);
					cv::circle(m_frame, lip, 5, cv::Scalar(255, 0, 0), 5);
				}

				if (rightLip)
				{
					cv::Point2f lip = cv::Point2f(rightLip->x * size.width, rightLip->y * size.height);
					cv::circle(m_frame, lip, 5, cv::Scalar(255, 255, 0), 5);
				}

				if (bottomLip)
				{
					cv::Point2f lip = cv::Point2f(bottomLip->x * size.width, bottomLip->y * size.height);
					cv::circle(m_frame, lip, 5, cv::Scalar(0, 255, 255), 5);
				}
			}
		}

		if (m_bDebugContext)
		{
			cv::imshow("Original", m_frame);
			cv::imshow("Mask", m_mask);

			/*cv::createTrackbar("loRed", "Original", &m_loCol[0], 255);
			cv::createTrackbar("loGreen", "Original", &m_loCol[1], 255);
			cv::createTrackbar("loBlue", "Original", &m_loCol[2], 255);
			cv::createTrackbar("hiRed", "Original", &m_hiCol[0], 255);
			cv::createTrackbar("hiGreen", "Original", &m_hiCol[1], 255);
			cv::createTrackbar("hiBlue", "Original", &m_hiCol[2], 255);*/
		}

		if (m_iUpdateRate > 0)
			cv::waitKey(m_iUpdateRate);
	}
}