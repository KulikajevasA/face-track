#ifndef FACE_TRACKER_WRAPPER_H
#define FACE_TRACKER_WRAPPER_H

#include "Contour.h"
#include "Color.h"
#include "FeatureSorter.h"
#include "TrackSettings.h"

namespace Tracker
{
	class FaceTrackerWrapper
	{
	private:
		void *m_pFaceTracker;
		FeatureSorter *m_pFeatureSorter;

	public:
		FaceTrackerWrapper(TrackSettingsT settings = TrackSettingsT());
		~FaceTrackerWrapper(void);
		void Start(void);
		void Stop(void);
		Contour GetLipTop(void) { return m_pFeatureSorter->GetFaceIndex(0); }
		Contour GetLipLeft(void) { return m_pFeatureSorter->GetFaceIndex(1); }
		Contour GetLipRight(void) { return m_pFeatureSorter->GetFaceIndex(2); }
		Contour GetLipBottom(void) { return m_pFeatureSorter->GetFaceIndex(3); }
	};
}
#endif