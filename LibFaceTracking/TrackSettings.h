#ifndef TRACK_SETTINGS_H
#define TRACK_SETTINGS_H

#include "Color.h"

namespace Tracker
{
	struct TrackSettingsT
	{
		int cameraIndex;
		Color loCol, hiCol;
		int updateRateMS;
		bool debug;

		TrackSettingsT()
		{
			cameraIndex = 0;
			loCol = Color(92, 138, 0);
			hiCol = Color(61, 22, 181);
			updateRateMS = 1;
			debug = false;
		}
	};
}

#endif