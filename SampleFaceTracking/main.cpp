#include "FaceTrackerWrapper.h"
#include <iostream>
#include <windows.h>


int main()
{
	Tracker::TrackSettingsT settings;
	settings.debug = true;
	auto tracker = Tracker::FaceTrackerWrapper(settings);
	tracker.Start();

	while (true)
	{
		printf("Cycle begin...\n");
		if (!tracker.GetLipTop().dead)
			printf("	Lip top: x%f y%f\n", tracker.GetLipTop().x, tracker.GetLipTop().y);

		if (!tracker.GetLipLeft().dead)
			printf("	Lip left: x%f y%f\n", tracker.GetLipLeft().x, tracker.GetLipLeft().y);

		if (!tracker.GetLipRight().dead)
			printf("	Lip right: x%f y%f\n", tracker.GetLipRight().x, tracker.GetLipRight().y);

		if (!tracker.GetLipBottom().dead)
			printf("	Lip bottom: x%f y%f\n", tracker.GetLipBottom().x, tracker.GetLipBottom().y);

		Sleep(100);
		printf("end.\n");
	}

	return 0;
}